package org.training.idc.selenium1;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NewTest {
	
	WebDriver driver;
  @BeforeTest
  public void first() {
	  driver=new FirefoxDriver();
	  driver.get("https://www.training-support.net");
	  System.out.println("Sample Test NG");
  }
  
  @Test(dependsOnMethods = {"three"})
  public void two()
  {
	  
	  System.out.println("Sample Test NG- two");
  }
  
  @Test()
  public void three()
  {
	  
	  System.out.println("Sample Test NG- three");
	  
	  System.out.println("Sample Test NG- three");
	  
	  System.out.println("Sample Test NG- three");
  }
  @Test(enabled = false)
  public void skipfour() 
  {
	  
	  System.out.println("Sample Test NG- four");
  }
  
  @Test()
  public void five()  throws SkipException
  {
	  
	  System.out.println("Sample Test NG- five");
	  
  }
  @AfterTest
  public void last()
  {
	  driver.close();
  }
}
