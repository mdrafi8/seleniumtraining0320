package org.training.idc.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity4day3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		WebDriverWait wait=new WebDriverWait(driver,10);
		driver.get("https://www.training-support.net/selenium/ajax");
		driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/button")).click();
		boolean test=wait.until((ExpectedConditions.textToBePresentInElementLocated(By.xpath("//*[@id=\"ajax-content\"]/h1"), "HELLO!")));
		System.out.println("Status"+test);
		
		driver.close();

	}

}
