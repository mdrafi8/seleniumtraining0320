package org.training.idc.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity3Day2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriver driver =new FirefoxDriver();
		
		driver.get("https://www.training-support.net/selenium/simple-form");
		driver.findElement(By.id("firstName")).sendKeys("Mohammed");
		driver.findElement(By.id("lastName")).sendKeys("Rafi");
		driver.findElement(By.id("email")).sendKeys("sample@oracle.com");
		driver.findElement(By.id("number")).sendKeys("9122324234");
		driver.findElement(By.tagName("textarea")).sendKeys("Hello World.");
		
		driver.findElement(By.xpath("//*[@id=\"simpleForm\"]/div/div[6]/div[1]/input")).click();
		
		Thread.sleep(10000);
		driver.close();
	}

}
