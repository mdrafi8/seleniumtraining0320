package org.training.idc.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Activity3Day3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver=new FirefoxDriver();
		WebDriverWait wait=new WebDriverWait(driver,10);
		driver.get("https://www.training-support.net/selenium/dynamic-controls");
		driver.findElement(By.id("toggleCheckbox")).click();
		boolean tes=wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath("//*[@id=\"dynamicCheckbox\"]/input"))));
		System.out.println("state"+tes); 
	}

}
