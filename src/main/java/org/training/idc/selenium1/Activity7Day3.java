package org.training.idc.selenium1;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity7Day3 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/javascript-alerts");
		driver.findElement(By.cssSelector("button#simple")).click();
		System.out.println(driver.getWindowHandle());
		
		System.out.println(driver.getWindowHandles());
		
		Alert simplealert=driver.switchTo().alert();
		
		
		
		String text=simplealert.getText();
		Thread.sleep(3000);
		System.out.println("ALret Text is "+text);
		simplealert.accept();
		
		Thread.sleep(5000);
		driver.close();
		
		
		
				
		

	}

}
