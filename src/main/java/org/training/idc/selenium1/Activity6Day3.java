package org.training.idc.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Activity6Day3 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/drag-drop");
		driver.manage().window().maximize();
		WebElement ball=driver.findElement(By.xpath("//img[@id=\"draggable\"]"));
		
		WebElement drop1=driver.findElement(By.xpath("//div[@id=\"droppable\"]"));
		
		WebElement drop2=driver.findElement(By.xpath("//div[@id=\"dropzone2\"]"));
		
		Actions act=new Actions(driver);
		act.dragAndDrop(ball, drop1).build().perform();
		
		Thread.sleep(3000);
		if(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/p")).isDisplayed())
			System.out.println("Droped in box1");
		
		Thread.sleep(3000);
		act.dragAndDrop(ball, drop2).build().perform();
		
		Thread.sleep(1000);
		
		
		if(driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/p")).isDisplayed())
		System.out.println("Droped in box2");
		
		Thread.sleep(5000);
		driver.close();
	}

}
