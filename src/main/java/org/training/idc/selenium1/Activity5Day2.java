package org.training.idc.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5Day2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		WebDriver driver=new FirefoxDriver();
		//driver.get("https://www.training-support.net/selenium/target-practice");
		driver.navigate().to("https://www.training-support.net/selenium/target-practice");
		System.out.println("Title"+driver.getTitle());
		
		WebElement h3= driver.findElement(By.id("third-header"));
				System.out.println(h3.getText());
				System.out.println(h3.getTagName());
				System.out.println(h3.getCssValue("color"));
		WebElement btn=	driver.findElement(By.cssSelector(".violet"));	
		System.out.println(btn.getCssValue("color"));	
		System.out.println(btn.isEnabled())	;	
		
		driver.navigate().to("https://www.training-support.net/selenium/dynamic-controls");
		
		WebElement text1=driver.findElement(By.id("input-text"));
		System.out.println("Before Enable:"+text1.isEnabled());
		driver.findElement(By.id("toggleInput")).click();
		System.out.println("After  Enable:"+text1.isEnabled());
		
		Thread.sleep(10000);
		WebElement chkbx=driver.findElement(By.name("toggled"));
		
		//Activity 5-2
		System.out.println("Before Select"+chkbx.isSelected());
		chkbx.click();
		
		System.out.println("After select "+chkbx.isDisplayed());
		Thread.sleep(10000);
		//Activity 5-1
		System.out.println("Before CLick"+chkbx.isDisplayed());
		driver.findElement(By.id("toggleCheckbox")).click();
		System.out.println(chkbx.isDisplayed());
		
		
				Thread.sleep(10000);
				driver.close();		
		
	}

}
