package org.training.idc.selenium1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity4Day2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.training-support.net");
		
		System.out.println("Title"+driver.getTitle());
		driver.findElement(By.tagName("A")).click();
		System.out.println("Title"+driver.getTitle());
		
		Thread.sleep(10000);
		driver.close();

	}

}
